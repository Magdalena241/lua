----------------------------------------------------
-- Constatns.lua
----------------------------------------------------

WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720
PADDLE_SPEED = 300
OBSTACLE_COLOR = {0,255,0,255}
BALL = "BALL"
BRICK = "BRICK"
OBSTACLE = "OBSTACLE"
PADDLE = "PADDLE"
BRICK_WIDTH = 60
BRICK_HEIGHT = 30
BRICK_SPACE = 0

----------------------------------------------------
-- Game.lua
----------------------------------------------------

LIVE = 3
GAME_RUN = false
GAME_OVER = false

function loseLife()
  LIVE = LIVE - 1
  if LIVE == 0 then GAME_OVER = true
  else resetPosition()
  end
end

function resetPosition()
  GAME_RUN = false
  objects.ball.body:setX(WINDOW_WIDTH / 2)
  objects.ball.body:setY(WINDOW_HEIGHT - 50)
  objects.ball.velocityDir = {x = -math.sqrt(2)/2 * 300, y =  -math.sqrt(2)/2 * 300}
  objects.paddle.body:setX(WINDOW_WIDTH / 2)
  objects.paddle.body:setY(WINDOW_HEIGHT - 30)
end

----------------------------------------------------
-- Paddle.lua
----------------------------------------------------

Paddle = {}
Paddle.__index = Paddle


function Paddle:create(world)
  
  local paddle = {}
  paddle.body = love.physics.newBody(world,WINDOW_WIDTH / 2, WINDOW_HEIGHT - 30, "dynamic")
  paddle.body:setFixedRotation(true)
  paddle.shape = love.physics.newRectangleShape(80, 10)
  paddle.fixture = love.physics.newFixture(paddle.body, paddle.shape)
  paddle.fixture:setUserData({name = PADDLE})
  setmetatable(paddle, Paddle)
  return paddle
end

function Paddle:draw() 
  love.graphics.setColor(255,255,255,255)
  love.graphics.polygon("fill", self.body:getWorldPoints(self.shape:getPoints()))
end

function Paddle:update(dt)
  if love.keyboard.isDown("right") then
    self.body:setLinearVelocity(PADDLE_SPEED, 0)
  elseif love.keyboard.isDown("left") then
    self.body:setLinearVelocity(-PADDLE_SPEED, 0)
  else
    self.body:setLinearVelocity(0, 0)
  end
end

----------------------------------------------------
-- Ball.lua
----------------------------------------------------

Ball = {}
Ball.__index = Ball

function Ball:create(world, xNumber, yNumber)
  local ball = {}
  ball.body = love.physics.newBody(world, WINDOW_WIDTH / 2, WINDOW_HEIGHT - 50, "dynamic")
  ball.shape = love.physics.newCircleShape(8)
  ball.fixture = love.physics.newFixture(ball.body, ball.shape)
  ball.fixture:setUserData({name = BALL})
  ball.velocityDir = {x = -math.sqrt(2)/2 * 300, y =  -math.sqrt(2)/2 * 300}
  setmetatable(ball, Ball)
  return ball
end

function Ball:update(dt)
  if GAME_RUN == true then
    local x = self.velocityDir.x
    local y = self.velocityDir.y
    self.body:setLinearVelocity(x,y)
    local xx, yy = self.body:getWorldCenter()
    if yy > WINDOW_HEIGHT then loseLife() end
  else 
    if love.keyboard.isDown("right") then
      self.body:setLinearVelocity(PADDLE_SPEED, 0)
    elseif love.keyboard.isDown("left") then
      self.body:setLinearVelocity(-PADDLE_SPEED, 0)
    else
      self.body:setLinearVelocity(0, 0)
    end
  end
end

function Ball:draw()
  love.graphics.setColor(255,255,255,255)
  love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.shape:getRadius())
end

----------------------------------------------------
-- Brick.lua
----------------------------------------------------

Brick = {}
Brick.__index = Brick

function Brick:create(world, xPos, yPos, x, y) 
  local brick = {}
  brick.body = love.physics.newBody(world, xPos, yPos - 30, "static")
  brick.body:setFixedRotation(true)
  brick.shape = love.physics.newRectangleShape(BRICK_WIDTH, BRICK_HEIGHT)
  brick.fixture = love.physics.newFixture(brick.body, brick.shape)
  brick.fixture:setUserData({ name = BRICK, x = x, y = y})
  brick.resistance = math.random(1, 3)
  setmetatable(brick, Brick)
  return brick
end

function Brick:draw() 
  local r = self.resistance
    if r > 0 then
    if r == 1 then love.graphics.setColor(255,0,0,255)
    elseif r == 2 then love.graphics.setColor(0,255,0,255)
    else love.graphics.setColor(0,0,255,255)
    end
    love.graphics.polygon("fill", self.body:getWorldPoints(self.shape:getPoints()))
    love.graphics.setColor(0,0,0,255)
    love.graphics.setLineWidth(8)
    love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
  end
end

----------------------------------------------------
-- Brick.lua
----------------------------------------------------

Obstacle = {}
Obstacle.__index = Obstacle

function Obstacle:create(world, xPos, yPos, width, height)
  local obstacle = {}
  obstacle.body = love.physics.newBody(world, xPos, yPos, "static")
  obstacle.shape = love.physics.newRectangleShape(width, height)
  obstacle.fixture = love.physics.newFixture(obstacle.body, obstacle.shape)
  obstacle.fixture:setUserData({name = OBSTACLE})
  setmetatable(obstacle, Obstacle)
  return obstacle
end

function Obstacle:draw()
  love.graphics.setColor(OBSTACLE_COLOR)
  love.graphics.polygon("fill", self.body:getWorldPoints(self.shape:getPoints()))
end

----------------------------------------------------
-- main.lua
----------------------------------------------------

function love.load(arg)
  love.window.setMode(WINDOW_WIDTH, WINDOW_HEIGHT)
  love.physics.setMeter(1)
  world = love.physics.newWorld(0, 0, true)
  world:setCallbacks(beginContact, endContact, preSolve, postSolve)
  objects = {}
  objects.paddle = Paddle:create(world)
  objects.ball = Ball:create(world)
  objects.obstacles = {}
  objects.obstacles.left = Obstacle:create(world, 0, WINDOW_HEIGHT / 2, 10, WINDOW_HEIGHT)
  objects.obstacles.top = Obstacle:create(world, WINDOW_WIDTH / 2, 0, WINDOW_WIDTH, 10)
  objects.obstacles.right = Obstacle:create(world, WINDOW_WIDTH, WINDOW_HEIGHT / 2, 10, WINDOW_HEIGHT)
  
  objects.bricks = {}
  
  local a = (WINDOW_WIDTH - (BRICK_SPACE + BRICK_WIDTH)*15)/2
  local b = (WINDOW_HEIGHT - (BRICK_SPACE + BRICK_HEIGHT)*7)/4
  
  for i=0,6 do
    objects.bricks[i] = {}
    for j=0,14 do
      objects.bricks[i][j] = Brick:create(world, j*(BRICK_SPACE + BRICK_WIDTH) + a, i*(BRICK_SPACE + BRICK_HEIGHT) + b, i, j)
    end
  end
  
 -- if arg[#arg] == "-debug" then require("mobdebug").start() end
end


function love.draw()
  if GAME_OVER == false then
    objects.paddle:draw()
    objects.ball:draw()
    objects.obstacles.left:draw()
    objects.obstacles.top:draw()
    objects.obstacles.right:draw()
    love.graphics.setColor(255,255,0,255)
    love.graphics.print("Lives: "..LIVE, 10, 10, 0, 3, 3)
    
    for i=0,#objects.bricks do
      for j=0,#objects.bricks[i] do
        objects.bricks[i][j]:draw()
      end
    end
  else 
    love.graphics.setColor(255,255,0,255)
    love.graphics.print("Game", 400, 100, 0, 10, 10)
    love.graphics.print("Over", 400, 300, 0, 10, 10)
  end
end

function love.update(dt)
  
  if GAME_RUN == false then
    if love.keyboard.isDown("space") then GAME_RUN = true end
  end
  
  objects.paddle:update(dt)
  objects.ball:update(dt)
  world:update(dt)
end

function beginContact(a, b, coll)
  if a:getUserData().name == BALL or b:getUserData().name == BALL then
    local nx, ny = coll:getNormal()
    local vx = objects.ball.velocityDir.x
    local vy = objects.ball.velocityDir.y
    if nx ~= 0 then vx = -vx
    elseif ny ~= 0 then vy = -vy
    end
    objects.ball.velocityDir = {x = vx , y = vy}
  end
  if a:getUserData().name == BRICK then handleBrickColision(a) 
  elseif b:getUserData().name == BRICK then handleBrickColision(b) 
  end
end
 
function endContact(a, b, coll)

end

function preSolve(a, b, coll)

end

function postSolve(a, b, coll, normalimpulse, tangentimpulse)

end

function handleBrickColision(brick)
  local x = brick:getUserData().x
  local y = brick:getUserData().y
  local r = objects.bricks[x][y].resistance
  objects.bricks[x][y].resistance = r - 1
  if r - 1 == 0 then objects.bricks[x][y].fixture:destroy() end
end
